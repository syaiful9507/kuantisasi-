﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kuantisasi
{
    public partial class Form1 : Form
    {
        Bitmap objBitmap;
        Bitmap objBitmap1;
        Bitmap objBitmap2;
        Bitmap objBitmap3;
        Bitmap objBitmap4;

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_load_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = objBitmap;
            }
        }

        private void btn16_Click(object sender, EventArgs e)
        {
             objBitmap1 = new Bitmap(objBitmap);
             for (int x = 0; x < objBitmap.Width; x++)
             for (int y = 0; y < objBitmap1.Height; y++)
            {
            Color w = objBitmap.GetPixel(x, y);
            int r = w.R;
            int g = w.G;
            int b = w.B;
            int xg = (int)((r + g + b) / 3);
            int xk = 16*(int)(xg/16);
            Color wb = Color.FromArgb(xk, xk, xk);
            objBitmap1.SetPixel(x, y, wb);
            }
             pictureBox3.Image = objBitmap1;
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            objBitmap2 = new Bitmap(objBitmap);
            for (int x = 0; x < objBitmap.Width; x++)
                for (int y = 0; y < objBitmap2.Height; y++)
                {
                    Color w = objBitmap.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 8 * (int)(xg / 8);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmap2.SetPixel(x, y, wb);
                }
            pictureBox2.Image = objBitmap2;
        }

        private void btn32_Click(object sender, EventArgs e)
        {
            objBitmap3 = new Bitmap(objBitmap);
            for (int x = 0; x < objBitmap.Width; x++)
                for (int y = 0; y < objBitmap3.Height; y++)
                {
                    Color w = objBitmap.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 32 * (int)(xg / 32);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmap3.SetPixel(x, y, wb);
                }
            pictureBox4.Image = objBitmap3;
        }

        private void btn64_Click(object sender, EventArgs e)
        {
            objBitmap4 = new Bitmap(objBitmap);
            for (int x = 0; x < objBitmap.Width; x++)
                for (int y = 0; y < objBitmap4.Height; y++)
                {
                    Color w = objBitmap.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 64 * (int)(xg / 64);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmap4.SetPixel(x, y, wb);
                }
            pictureBox5.Image = objBitmap4;
        }
    }
}
